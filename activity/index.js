const http = require('http')
const port = 3000

const activityServer = http.createServer((request, response) => {
  if (request.url == '/login') {
    response.writeHead(200, { 'Content-type': 'text/plain' })
    response.end(`Welcome to the login page, user!`)
  } else {
    response.writeHead(404, { 'Content-type': 'text/plain' })
    response.end(`404: Page not found!`)
  }
})

activityServer.listen(port)

console.log(`Server is successfully running via port │█║▌║▌║${port}║▌║▌║█│`)
