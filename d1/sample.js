let http = require('http')
/*
  * require()
     is a built in JavaScript method which allows us to import packages
     * packages
        - are pieces of code we can integrate into our application
  * http
    - is a default package that comes from Node.js. It allows us to use methods that let us create servers
    http is a module. 
      * modules
        - are objects that contain codes, methods, or data   
  > protocol to client-server communication - http://localhost:6969 - server/applications
*/
http
  .createServer((request, response) => {
    /*
      * createServer() method 
        - is a method from the http module that allows us to handle requests and responses from a client and a server respectively
        - the request object contains details of the request from the client
        - the response object contains details for the response from the server
        - the createServer() method always receives the request object first before the response

      * response.writeHead()
        - is a method of the response object. It allows us to add:
          * headers
              - are additional information about our response
              > we have two arguments in our writeHead() method:
                * 1) HTTP status code
                    - is just a numerical code that let the client know about the status of their request
                    - 200 means OK
                    - 404 means the resource cannot be found
                * 2) "Content-Type"
                    - is one of the most recognizable headers. 
                    - simply pertains to the data type of our resonse
      * resonse.end()
        - ends our response
        - it is able to send a message/data as a string
    */
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end('Hola Amigo! 🤠🎺 Where have you been?! 🥸')
  })
  .listen(6969)
/*
    * .listen() 
      - allows us to assign a port to our server
      - this will allow us to serve our file (sample.js) server in our location machine assigned to our port 5000
      - there are several tasks and processess on our computer that run on different port numbers
      
      - hypertext transfer protocol => http://localhost:6969/ => server
      - localhost: => Local Machine: 
      - 6969 current port assigned to our server
      4000, 4040, 8000, 5000, 3000, 4200 - usually used for web development
  */

console.log('Server is running on localhost: 👉6969!💪🥳👍')
// we used console.log to show validation from which port number our server is running on
