/*
 * What is a client?
    - its an application which creates requests for resources from a server.
    - a client will trigger an action, in the web development context, through a URL and a wait for the response of the server

 * What is a server?
    - is able to host and deliver resources requested by a client. In fact, a single server can handle multiple client

 * What is Node.js?
    - a runtime environment which allows us to create or develop backend or server-side applications with JavaScript. Because by default, JavaScript was conceptualized solely to the front end.
     * Runtime Environment
        - is the environment in which a program or application is executed

 * Why is Node.js popular?
    * performance
        - Node.js is one of the best performing environment for creating backend applications with JavaScript
    * familiarity
        - since Node.js is built and uses JavaScript as its language, it is very familiar for most developers
    * Node Package Manager (NPM)
        - is the largest registry for node packages
          * What are packages?
              - are bits of programs, methods, functions, codes that greatly help in the development of an application
*/
const http = require('http')

const port = 4000

const server = http.createServer((request, response) => {
  if (request.url == '/greeting') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end('Hola Amigo! (^v^) Where have you been?!')
  } else if (request.url == '/homepage') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end(`Homepage ⌂`)
  } else {
    // Mini Activity #1
    // Create an else condition that all other routes will return a message of "Page not Available:"
    response.writeHead(404, { 'Content-Type': 'text/plain' })
    response.end('Webpage not available :(')
  }
})

server.listen(port)

console.log(`Server is now accessible via localhost: 👉${port} 🥸`)
